r"""
A library to perform M4DB web service requests involving geometries.
"""
import os
import base64
import json

from m4db_client.global_config import get_global_variables

from m4db_client.http_session import M4DBHttpServiceType
from m4db_client.http_session import M4DBHttpWithTicketHashService


class M4DBGeometryException(Exception):
    pass


class M4DBGeometryUniqueIdException(M4DBGeometryException):
    def __init__(self, message):
        self.message = message


class M4DBGeometryOutputFileException(M4DBGeometryException):
    def __init__(self, message):
        self.message = message


class M4DBGeometryNoMeshesException(M4DBGeometryException):
    def __init__(self, message):
        self.message = message


class M4DBGeometryUnrecognizedUnitException(M4DBGeometryException):
    def __init__(self, message):
        self.message = message


class M4DBGeometryUnrecognizedSizeConventionException(M4DBGeometryException):
    def __init__(self, message):
        self.message = message


class M4DBGeometryFileNotFound(M4DBGeometryException):
    def __init__(self, message):
        self.message = message


class M4DBGeometryNoNElements(M4DBGeometryException):
    pass


class M4DBGeometryNoNVertices(M4DBGeometryException):
    pass


class M4DBGeometryNoNSubmeshes(M4DBGeometryException):
    pass


class GetGeometryByUniqueId(M4DBHttpWithTicketHashService):

    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        if args.get("unique_id") is None:
            raise M4DBGeometryUniqueIdException("Variable 'unique_id' is required")

        endpoint = "https://{url:}:{port:}/geometry/{unique_id:}".format(
            url=gcfg.url,
            port=gcfg.port,
            unique_id=args["unique_id"]
        )

        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        return json.loads(http_response.text)


class GetAllGeometries(M4DBHttpWithTicketHashService):

    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        endpoint = "https://{url:}:{port:}/geometry/all".format(
            url=gcfg.url,
            port=gcfg.port
        )

        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        return json.loads(http_response.text)


class GetGeometryZipByUniqueId(M4DBHttpWithTicketHashService):
    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        if args.get("unique_id") is None:
            raise M4DBGeometryUniqueIdException("Variable 'unique_id' is required")
        if args.get("output_file") is None:
            raise M4DBGeometryOutputFileException("Variable 'output_file' is required")

        endpoint = "https://{url:}:{port:}/geometry/zip/{unique_id}".format(
            url=gcfg.url,
            port=gcfg.port,
            unique_id=args.get("unique_id")
        )

        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        # Write the file
        with open(args.get("output_file"), "wb") as fout:
            for chunk in http_response.iter_content(chunk_size=1024):
                fout.write(chunk)


class AddGeometry(M4DBHttpWithTicketHashService):
    def __init__(self):
        super().__init__(M4DBHttpServiceType.POST)

    def post(self, ticket_hash, **args):
        gcfg = get_global_variables()

        endpoint = "https://{url:}:{port:}/geometry/add".format(
            url=gcfg.url,
            port=gcfg.port
        )

        if args.get("patran_file") is None and args.get("exodus_file") is None:
            raise M4DBGeometryNoMeshesException("No patran or exodus files")

        if args.get("size_unit") not in gcfg.size_unit_symbols:
            raise M4DBGeometryUnrecognizedUnitException(args.get("size_unit"))

        if args.get("size_convention") not in gcfg.size_convention_symbols:
            raise M4DBGeometryUnrecognizedSizeConventionException(args.get("size_convention"))

        if args.get("element_size_unit") is not None and args.get("element_size_unit") not in gcfg.size_unit_symbols:
            raise M4DBGeometryUnrecognizedUnitException(args.get("element_size_unit"))

        if args.get("nelements") is None:
            raise M4DBGeometryNoNElements()

        if args.get("nvertices") is None:
            raise M4DBGeometryNoNVertices()

        if args.get("nsubmeshes") is None:
            raise M4DBGeometryNoNSubmeshes()

        if args.get("patran_file") is not None and not os.path.isfile(args.get("patran_file")):
            raise M4DBGeometryFileNotFound(args.get("patran_file"))

        if args.get("exodus_file") is not None and not os.path.isfile(args.get("exodus_file")):
            raise M4DBGeometryFileNotFound(args.get("exodus_file"))

        if args.get("mesh_gen_script_file") is not None and not os.path.isfile(args.get("mesh_gen_script_file")):
            raise M4DBGeometryFileNotFound(args.get("mesh_gen_script_file"))

        if args.get("mesh_gen_output_file") is not None and not os.path.isfile(args.get("mesh_gen_output_file")):
            raise M4DBGeometryFileNotFound(args.get("mesh_gen_output_file"))

        post_data = {
            "name": args.get("name"),
            "size": args.get("size"),
            "size_unit": args.get("size_unit"),
            "size_convention": args.get("size_convention"),
            "nelements": args.get("nelements"),
            "nvertices": args.get("nvertices"),
            "nsubmeshes": args.get("nsubmeshes")
        }

        if args.get("element_size") is not None:
            post_data["element_size"] = args.get("element_size")

        if args.get("element_size_unit") is not None:
            post_data["element_size_unit"] = args.get("element_size_unit")

        if args.get("description") is not None:
            post_data["description"] = args.get("description")

        if args.get("volume_total") is not None:
            post_data["volume_total"] = args.get("volume_total")

        if args.get("software_name") is not None:
            post_data["software_name"] = args.get("software_name")

        if args.get("software_version") is not None:
            post_data["software_version"] = args.get("software_version")

        if args.get("patran_file") is not None:
            with open(args.get("patran_file"), "rb") as fin:
                patran_data = base64.b64encode(fin.read()).decode("ascii")
                post_data["patran_data"] = patran_data

        if args.get("exodus_file") is not None:
            with open(args.get("exodus_file"), "rb") as fin:
                exodus_data = base64.b64encode(fin.read()).decode("ascii")
                post_data["exodus_data"] = exodus_data

        if args.get("mesh_gen_script_file") is not None:
            with open(args.get("mesh_gen_script_file"), "rb") as fin:
                mesh_gen_script_data = base64.b64encode(fin.read()).decode("ascii")
                post_data["mesh_gen_script_data"] = mesh_gen_script_data

        if args.get("mesh_gen_output_file") is not None:
            with open(args.get("mesh_gen_output_file"), "rb") as fin:
                mesh_gen_output_data = base64.b64encode(fin.read()).decode("ascii")
                post_data["mesh_gen_output_data"] = mesh_gen_output_data

        http_response = self.http_session.post(
            endpoint,
            verify=self.verify_services,
            headers={"Content-Type": "application/json", "user_name": gcfg.user_name, "ticket_hash": ticket_hash},
            data=json.dumps(post_data)
        )
        http_response.raise_for_status()

        return json.loads(http_response.text)
