r"""
Set up and retrieve the environment in which our scripts will run.
"""
import json
import os
import re

from m4db_client.decorators import static

from m4db_client.configuration import M4DBConfigurationMissingParameterException
from m4db_client.configuration import M4DBConfigurationNoEnvironmentVariableSetException
from m4db_client.configuration import read_config_from_environ

DATE_TIME_FORMAT = "%m/%d/%Y, %H:%M:%S"

STR_REGEX_MATERIAL_NAME = r"[a-zA-Z]+"
STR_REGEX_SIMPLE_FLOAT = r"[0-9]+(\.[0-9]+)?"


class Configuration:
    def __init__(self):

        config = read_config_from_environ()

        self.backoff = config["backoff"]
        self.forcelist = config["forcelist"]
        self.port = config["port"]
        self.retries = config["retries"]
        self.url = config["url"]
        self.work_dir = config["work_dir"]
        self.user_name = config["user_name"]

        # Check that each of the configuration variables is defined.
        self.check_config_defined()

        # Geometry directories
        self.geometry_zip_dir = os.path.join(self.work_dir, "geometry_archive")
        self.geometry_dir = os.path.join(self.work_dir, "geometry")

        # Model directories
        self.model_zip_dir = os.path.join(self.work_dir, "model_archive")
        self.model_dir = os.path.join(self.work_dir, "model")

        # NEB directories
        self.neb_zip_dir = os.path.join(self.work_dir, "neb_archive")
        self.neb_dir = os.path.join(self.work_dir, "neb")

        # The file that contains our current ticket
        self.ticket_hash_file = os.path.join(self.work_dir, "ticket_hash")

        # Acceptable unit symbols
        self.size_unit_symbols = ["m", "cm", "mm", "um", "pm", "fm", "am"]

        # Acceptable size conventions
        self.size_convention_symbols = ["ESVD", "ECVL"]

        # Acceptable running status names
        self.running_status_names = ["not-run", "re-run", "running", "finished", "crashed", "scheduled"]

        # Check that the work directory exists, if not then create it.
        if not os.path.isdir(self.work_dir):
            os.makedirs(self.work_dir, exist_ok=True)

            # Additionally create all the subdirectories
            os.makedirs(self.geometry_zip_dir, exist_ok=True)
            os.makedirs(self.geometry_dir, exist_ok = True)
            os.makedirs(self.model_zip_dir, exist_ok = True)
            os.makedirs(self.model_dir, exist_ok = True)
            os.makedirs(self.neb_zip_dir, exist_ok = True)
            os.makedirs(self.neb_dir, exist_ok = True)

            # Create an 'empty' ticket_hash file
            with open(self.ticket_hash_file, "w") as fout:
                fout.write(json.dumps(
                    {
                        "ticket_hash": None,
                        "ticket_timeout": None,
                        "ticket_issued": None
                    }
                ))

        # Regular expressions
        self.regex_material_temperature = re.compile(
            r"({material_name:})@({temperature:})C".format(
                material_name=STR_REGEX_MATERIAL_NAME,
                temperature=STR_REGEX_SIMPLE_FLOAT
            )
        )

    def check_config_defined(self):
        r"""
        Checks that the main configuration variables are defined.
        :return: None
        """
        if self.backoff is None:
            raise ValueError("Variable 'backoff' in configuration is not set.")
        if self.forcelist is None:
            raise ValueError("Variable 'forcelist' in configuration is not set.")
        if self.port is None:
            raise ValueError("Variable, 'port' in configuration is not set.")
        if self.retries is None:
            raise ValueError("Variable, 'retries' in configuration is not set.")
        if self.url is None:
            raise ValueError("Variable, 'url' in configuration is not set.")
        if self.work_dir is None:
            raise ValueError("Variable, 'work_dir' in configuration is not set.")


@static(gcfg=None)
def get_global_variables():
    if get_global_variables.gcfg is None:
        get_global_variables.gcfg = Configuration()
    return get_global_variables.gcfg