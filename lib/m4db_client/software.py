r"""
A library to perform M4DB web service requests involving software.
"""
import re
import json

from m4db_client.global_config import get_global_variables

from m4db_client.http_session import M4DBHttpServiceType
from m4db_client.http_session import M4DBHttpWithTicketHashService

class M4DBSoftwareException(Exception):
    r"""
    Base exception for all software exceptions.
    """
    pass


class M4DBSoftwareParamException(Exception):
    def __init__(self, message):
        self.message = message


class GetAllSoftware(M4DBHttpWithTicketHashService):
    r"""
    Objects of this class will retrieve a model with a
    specific unique identifier.
    """

    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        endpoint = "https://{url:}:{port:}/software/all".format(
            url=gcfg.url,
            port=gcfg.port
        )

        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        return json.loads(http_response.text)

def main():
    parser = get_command_line_parser()

    args = parser.parse_args()

    if args.action == "show":
        show_action(args)
    elif args.action == "update":
        update_action(args)
    elif args.action is None:
        parser.print_help()
    else:
        print("Unknown action '{}'".format(args.action))


if __name__ == "__main__":
    main()