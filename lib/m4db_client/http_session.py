r"""
A library to retrieve an http session object.
"""
import os
import json
import sys
import getpass
import requests

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from requests.packages.urllib3 import disable_warnings

from requests import HTTPError
from requests.exceptions import RetryError

from enum import Enum

from m4db_client.global_config import get_global_variables

from m4db_client.utilities import str_to_datetime

from datetime import datetime


class M4DBHttpServiceException(Exception):
    r"""
    Base class for M4DB http exceptions.
    """
    pass


class M4DBHttpInvalidTicketException(M4DBHttpServiceException):
    def __init__(self, message):
        self.message = message


class M4DBHttpTicketFileMissingException(M4DBHttpServiceException):
    def __init__(self, message):
        self.message = message


class M4DBHttpServiceType(Enum):
    GET = 0
    POST = 1


class M4DBHttpService:
    r"""
    THis is the parent class of all http services.
    """

    def __init__(self, type, verify_services=False):
        disable_warnings(InsecureRequestWarning)

        gcfg = get_global_variables()

        self.type = type
        self.verify_services = verify_services

        # Create a force list of http codes that will not be retried.
        self.force_list = [
            int(f.strip()) for f in gcfg.forcelist.split(',')
        ]

        # Create a http retry object.
        self.retry = Retry(
            total=int(gcfg.retries),
            read=int(gcfg.retries),
            connect=int(gcfg.retries),
            status_forcelist=self.force_list
        )

        # Create an HttpAdapter object
        self.adapter = HTTPAdapter(max_retries=self.retry)

        # Create an http session object
        self.http_session = requests.Session()
        self.http_session.mount("https://", adapter=self.adapter)

    def communicate(self, **args):
        pass


class M4DBHttpWithoutTicketHashService(M4DBHttpService):
    r"""
    This is the parent class for all services that do not require ticket_hashes stored on the local file system.
    """
    def __init__(self, type, verify_services=False):
        super().__init__(type, verify_services)

    def communicate(self, **args):
        if self.type == M4DBHttpServiceType.GET:
            return self.get(**args)
        elif self.type == M4DBHttpServiceType.POST:
            return self.post(**args)

    def get(self, **args):
        r"""
        :param args:
        :return:
        """
        pass

    def post(self, **args):
        r"""
        """
        pass


class M4DBHttpWithTicketHashService(M4DBHttpService):

    def __init__(self, type, verify_services=False):
        super().__init__(type, verify_services)

    def get_ticket_hash(self):
        r"""
        This function verifies our ticket - i.e. is the ticket out of date.
        :return:
        """
        gcfg = get_global_variables()

        # If the ticket hash file does not exist then we don't have a valid ticket.
        if not os.path.isfile(gcfg.ticket_hash_file):
            raise M4DBHttpTicketFileMissingException("The ticket hash file does not exist")

        # Read the ticket hash file.
        with open(gcfg.ticket_hash_file, "r") as fin:
            ticket_data = json.load(fin)

        # Check that the ticket information is not all null
        if ticket_data["ticket_hash"] is None:
            raise M4DBHttpInvalidTicketException("The ticket has no ticket hash info")
        if ticket_data["ticket_timeout"] is None:
            raise M4DBHttpInvalidTicketException("The ticket has no time out info")
        if ticket_data["ticket_issued"] is None:
            raise M4DBHttpInvalidTicketException("The ticket has no issue time info")

        # If the ticket hash is not out of date, then use the ticket hash
        now = datetime.now()
        ticket_timeout = str_to_datetime(ticket_data["ticket_timeout"])
        if ticket_timeout < now:
            raise M4DBHttpInvalidTicketException("The ticket has timed out")

        # Return the ticket hash
        return ticket_data["ticket_hash"]

    def update_ticket(self, password):
        r"""
        This function will update our ticket data file.
        :return:
        """
        gcfg = get_global_variables()

        try:
            ticket_endpoint = "https://{url:}:{port:}/user/ticket-hash".format(
                url=gcfg.url,
                port=gcfg.port
            )
            ticket_response = self.http_session.get(
                ticket_endpoint,
                verify=self.verify_services,
                headers={"user_name": gcfg.user_name, "password": password}
            )
            ticket_response.raise_for_status()

            if not os.path.isfile(gcfg.ticket_hash_file):
                raise M4DBHttpTicketFileMissingException("The ticket hash file does not exist")

            with open(gcfg.ticket_hash_file, "w") as fout:
                fout.write(ticket_response.text)

        except HTTPError as http_error:
            print("There was an error trying to retrieve data")
            status_code = http_error.response.status_code
            if status_code == 401:
                print("-- authorization failed")
                sys.exit(1)
            else:
                print("-- HTTP error occured (code: {})".format(status_code))
                sys.exit(1)
        except RetryError as http_except:
            print("Maximum number of retries exceeded while attempting to authenticate")
            sys.exit(1)

    def communicate(self, **args):
        gcfg = get_global_variables()
        ticket_hash = self.get_ticket_hash()
        if self.type == M4DBHttpServiceType.GET:
            return self.get(ticket_hash, **args)
        elif self.type == M4DBHttpServiceType.POST:
            return self.post(ticket_hash, **args)

    def get(self, ticket_hash, **args):
        r"""
        Derived objects must implement this method.
        :param ticket_hash:
        :param user_name:
        :param params:
        :return:
        """
        pass

    def post(self, ticket_hash, **args):
        r"""
        Derived objects must implement this method.
        :param ticket_hash:
        :param user_name:
        :param params:
        :return:
        """
        pass


def wrap_http_communicate(http_comm_obj, itr=0, max_itr=1, **args):
    r"""
    Helper utility function wraps http communications, if a ticket is not valid, then
    the user is asked to authenticate a new ticket max_itr is the maximum no of attempts
    a user may have to authenticate, this function should *NOT THROW OR RETHROW* any exceptions!!!
    :param http_comm_obj:
    :param args:
    :param itr:
    :param max_itr:
    :return:
    """
    gcfg = get_global_variables()
    try:
        result = http_comm_obj.communicate(**args)
        return result, True
    except M4DBHttpInvalidTicketException as e:
        # The ticket is invalid, ask the user for their password to get a new ticket.
        print("Your ticket is not valid, please input your password for a new ticket")
        password = getpass.getpass(prompt="Password: ")

        http_comm_obj.update_ticket(password)

        if itr < max_itr:
            return wrap_http_communicate(http_comm_obj, itr=itr + 1, **args)
        else:
            print("Failed too many times to get a ticket")
            return [], False
    except M4DBHttpTicketFileMissingException as e:
        # The ticket hash file is missing, ask the user to recreate
        choice = input("Your ticket hash file does not exist, should I create it for you? (y/n)? ")
        if choice.lower() == "y":
            if os.path.isdir(gcfg.work_dir):
                os.makedirs(gcfg.work_dir, exist_ok=True)
            with open(gcfg.ticket_hash_file, "w") as fout:
                fout.write(json.dumps({
                    "ticket_hash": None,
                    "ticket_timeout": None,
                    "ticket_issued": None
                }))
            # Go around again but don't increment the iteration
            return wrap_http_communicate(http_comm_obj, itr, **args)
        else:
            return [], False
    except HTTPError as http_error:
        if http_error.response.status_code == 401:
            # The ticket is invalid, ask the user for their password to get a new ticket.
            print("Your ticket is no longer valid, please input your password for a new ticket")
            password = getpass.getpass(prompt="Password: ")
            http_comm_obj.update_ticket(password)
            if itr < max_itr:
                return wrap_http_communicate(http_comm_obj, itr=itr + 1, **args)
            else:
                print("Failed too many times to get a ticket")
                return [], False
        else:
            print("Failed to execute: (code: {})".format(http_error.response.status_code))
            return [], False

