r"""
A library to perform M4DB web service requests involving users.
"""
import json
import uuid
from hashlib import md5

from m4db_client.global_config import get_global_variables

from m4db_client.http_session import M4DBHttpServiceType
from m4db_client.http_session import M4DBHttpWithTicketHashService
from m4db_client.http_session import M4DBHttpWithoutTicketHashService


class M4DBUserException(Exception):
    r"""
    Base exception for all geometry exceptions.
    """
    pass


class M4DBUserNameException(M4DBUserException):
    r"""
    Exception raised based on user name errors.
    """
    def __init__(self, message):
        self.message = message


class M4DBUserPasswordException(M4DBUserException):
    r"""
    Exception raised based on user password.
    """
    def __init__(self, message):
        self.message = message
        
class M4DBUserFirstNameException(M4DBUserException):
    r"""
    Exception raised on user first name errors.
    """
    def __init__(self, message):
        self.message = message


class M4DBUserSurnameException(M4DBUserException):
    r"""
    Exception raised on user surname errors.
    """
    def __init__(self, message):
        self.message = message


class M4DBUserEmailException(M4DBUserException):
    r"""
    Exception raised on user email errors.
    """
    def __init__(self, message):
        self.message = message


class M4DBUserTicketLengthException(M4DBUserException):
    r"""
    Exception raised on user ticket length errors.
    """
    def __init__(self, message):
        self.message = message


class M4DBUserAccessLevelException(M4DBUserException):
    r"""
    Exception raised on user access level errors.
    """
    def __init__(self, message):
        self.message = message


class M4DBUserTicketHashException(M4DBUserException):
    r"""
    Exception raised on user ticket hash errors.
    """
    def __init__(self, message):
        self.message = message


class GetAllUsers(M4DBHttpWithTicketHashService):
    r"""
    Class to use the GetAllUsers web service.
    """

    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        endpoint = "https://{url:}:{port:}/user/all".format(
            url=gcfg.url,
            port=gcfg.port
        )

        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        return json.loads(http_response.text)


class NewUser(M4DBHttpWithTicketHashService):
    r"""
    Class to use the NewUser web servive.
    """

    def __init__(self):
        super().__init__(M4DBHttpServiceType.POST)

    def post(self, ticket_hash, **args):
        gcfg = get_global_variables()

        if args.get("user_name") is None:
            raise M4DBUserNameException("Variable 'user_name' is required")
        if args.get("first_name") is None:
            raise M4DBUserFirstNameException("Variable 'first_name' is required")
        if args.get("surname") is None:
            raise M4DBUserSurnameException("Variable 'surname' is required")
        if args.get("email") is None:
            raise M4DBUserEmailException("Variable 'email' is required")
        if args.get("ticket_length") is None:
            raise M4DBUserTicketLengthException("Variable 'ticket_length' is required")
        if args.get("access_level") is None:
            raise M4DBUserAccessLevelException("Variable 'access_length' is required")

        endpoint = "https://{url:}:{port:}/user/new".format(
            url=gcfg.url,
            port=gcfg.port
        )

        post_data = {
            "user_name": args.get("user_name"),                    # Req
            "password": md5(str(uuid.uuid4()).encode("ascii")).hexdigest(),
            "first_name": args.get("first_name"),                  # Req
            "initials": args.get("initials"),                      # Optional
            "surname": args.get("surname"),                        # Req
            "email": args.get("email"),                            # Req
            "telephone": args.get("telephone"),                    # Optional
            "ticket_length": args.get("ticket_length"),            # Req
            "access_level": args.get("access_level"),              # Req
        }

        http_response = self.http_session.post(
            endpoint,
            verify=self.verify_services,
            headers={
                "Content-Type": "application/json",
                "user_name": gcfg.user_name,
                "ticket_hash": ticket_hash
            },
            data=json.dumps(post_data)
        )
        http_response.raise_for_status()


class ForgottenPassword(M4DBHttpWithoutTicketHashService):
    r"""
    Class to use the ForgottenPassword web service.
    """
    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, **args):
        gcfg = get_global_variables()

        endpoint = "https://{url:}:{port:}/user/forgotten?user_name={user_name:}".format(
            url=gcfg.url,
            port=gcfg.port,
            user_name=gcfg.user_name
        )

        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services
        )
        http_response.raise_for_status()


class TicketHash(M4DBHttpWithoutTicketHashService):
    r"""
    Class to use the HashTicket web service.
    """

    def __init__(self, auto_update_ticket_hash_file=True):
        self.auto_update_ticket_hash_file=auto_update_ticket_hash_file
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, **args):

        if args.get("password") is None:
            raise M4DBUserPasswordException("Variable 'password' is required")

        gcfg = get_global_variables()

        endpoint = "https://{url:}:{port:}/user/ticket-hash".format(
            url=gcfg.url,
            port=gcfg.port
        )

        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "password": args.get("password")}
        )
        http_response.raise_for_status()

        if self.auto_update_ticket_hash_file:
            with open(gcfg.ticket_hash_file, "w") as fout:
                # If there were no exceptions, just write the response
                # to the user's ticket hash file.
                fout.write(http_response.text)

        return json.loads(http_response.text)


class ChangePassword(M4DBHttpWithoutTicketHashService):
    r"""
    Class to use the ChangePassword service.
    """

    def __init__(self):
        super().__init__(M4DBHttpServiceType.POST)

    def post(self, **args):
        gcfg = get_global_variables()

        if args.get("ticket_hash") is None:
            raise M4DBUserTicketHashException("Variable 'ticket_hash' required")
        if args.get("new_password") is None:
            raise M4DBUserPasswordException("Variabl 'new_password' is required")

        endpoint = "https://{url:}:{port:}/user/change-password".format(
            url=gcfg.url,
            port=gcfg.port
        )

        http_response = self.http_session.post(
            endpoint,
            verify=self.verify_services,
            headers={
                "Content-Type": "application/json",
                "user_name": gcfg.user_name,
                "ticket_hash": args.get("ticket_hash")
            },
            data=json.dumps({"new_password": args.get("new_password")})
        )
        http_response.raise_for_status()
