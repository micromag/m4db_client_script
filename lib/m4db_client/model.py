r"""
A library to perform M4DB web service requests involving models.
"""

import json

from m4db_client.global_config import get_global_variables

from m4db_client.http_session import M4DBHttpServiceType
from m4db_client.http_session import M4DBHttpWithTicketHashService


class M4DBModelException(Exception):
    r"""
    Base exception for all model exceptions.
    """
    pass


class M4DBModelParamException(Exception):
    def __init__(self, message):
        self.message = message


class M4DBModelUniqueIdException(M4DBModelException):
    r"""
    Exception raised based on model unique id errors.
    """
    def __init__(self, message):
        self.message = message


class M4DBModelOutputFileException(M4DBModelException):
    r"""
    Exception raised based on model output file errors.
    """
    def __init__(self, message):
        self.message = message


class GetModelByUniqueId(M4DBHttpWithTicketHashService):
    r"""
    Objects of this class will retrieve a model with a
    specific unique identifier.
    """

    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        if args.get("unique_id") is None:
            raise M4DBModelUniqueIdException("Variable 'unique_id' is required")

        endpoint = "https://{url:}:{port:}/model/{unique_id:}".format(
            url=gcfg.url,
            port=gcfg.port,
            unique_id=args.get("unique_id")
        )

        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        return json.loads(http_response.text)


class GetModelZipByUniqueId(M4DBHttpWithTicketHashService):
    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        if args.get("unique_id") is None:
            raise M4DBModelUniqueIdException("Variable 'unique_id' is required")
        if args.get("output_file") is None:
            raise M4DBModelOutputFileException("Variable 'output_file' is required")

        endpoint = "https://{url:}:{port:}/model/zip/{unique_id:}".format(
            url=gcfg.url,
            port=gcfg.port,
            unique_id=args.get("unique_id")
        )
        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        # Write the file
        with open(args.get("output_file"), "wb") as fout:
            for chunk in http_response.iter_content(chunk_size=1024):
                fout.write(chunk)


class GetModelQuantsByUniqueId(M4DBHttpWithTicketHashService):
    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        if args.get("unique_id") is None:
            raise M4DBModelUniqueIdException("Variable 'unique_id' is required")

        endpoint = "https://{url:}:{port:}/model/quants/{unique_id:}".format(
            url=gcfg.url,
            port=gcfg.port,
            unique_id=args.get("unique_id")
        )
        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        return json.loads(http_response.text)


########################################################################################################################
# GetDataByMGST                                                                                                        #
########################################################################################################################


class GetDataByMGST(M4DBHttpWithTicketHashService):

    def __init__(self, base_url=None):
        super().__init__(M4DBHttpServiceType.GET)

        if base_url is None:
            raise ValueError("GetDataByMGST class has no 'base_url' to communicate with")
        else:
            self.base_url = base_url

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        if args.get("running_status") is None:
            raise M4DBModelParamException("Variable 'running_status' is required")
        if args.get("project_name") is None:
            raise M4DBModelParamException("Variable 'project_name' is required")
        if args.get("geometry") is None:
            raise M4DBModelParamException("Variable 'geometry' is required")
        if args.get("size") is None:
            raise M4DBModelParamException("Variable 'size' is required")
        if args.get("size_unit") is None:
            raise M4DBModelParamException("Variable 'size_unit' is required")
        if args.get("size_convention") is None:
            raise M4DBModelParamException("Variable 'size_convention' is required")

        # If we're not searching a specific user's models, then use the current user_name
        if args.get("user_name") is None:
            user_name = gcfg.user_name
        else:
            user_name = args.get("user_name")

        # If there are no materials then don't process them
        params = {}
        if args.get("materials") is not None:
            if args.get("temperatures") is None:
                raise M4DBModelParamException("Variable 'materials' supplied but no 'temperatures' given")

            materials = args.get("materials")
            temperatures = args.get("temperatures")

            if len(materials) != len(temperatures):
                raise M4DBModelParamException("Lists 'materials' and 'temperatures' must have same length")

            for index, (material, temperature) in enumerate(zip(materials, temperatures)):
                params["material{}".format(index)] = material
                params["temperature{}".format(index)] = temperature

        endpoint = "https://{url:}:{port:}/{base_url:}/{running_status:}/{user_name:}/{project_name:}/{geometry:}/{size:}/{size_unit:}/{size_convention:}".format(
            url=gcfg.url,
            port=gcfg.port,
            running_status=args.get("running_status"),
            user_name=user_name,
            project_name=args.get("project_name"),
            geometry=args.get("geometry"),
            size=args.get("size"),
            size_unit=args.get("size_unit"),
            size_convention=args.get("size_convention"),
            base_url=self.base_url
        )
        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash},
            params=params
        )
        http_response.raise_for_status()

        return json.loads(http_response.text)

########################################################################################################################
# Implement the actual end points.
########################################################################################################################


class GetModelsQuantsByMGST(GetDataByMGST):
    def __init__(self):
        super().__init__("models/quants")


class GetModelsByMGST(GetDataByMGST):
    def __init__(self):
        super().__init__("models")


class GetModelsUniqueIdsByMGST(GetDataByMGST):
    def __init__(self):
        super().__init__("models/unique-ids")

########################################################################################################################
# Retrieve model information by unique id
########################################################################################################################


class GetModelMagnetizationByUniqueId(M4DBHttpWithTicketHashService):
    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        if args.get("unique_id") is None:
            raise M4DBModelParamException("Variable 'unique_id' is required")

        endpoint = "https://{url:}:{port:}/model/magnetization/{unique_id:}".format(
            url=gcfg.url,
            port=gcfg.port,
            unique_id=args.get("unique_id")
        )
        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        return json.loads(http_response.text)


########################################################################################################################
# Retrieve some summary data
########################################################################################################################


class GetModelsSummaryByUserProject(M4DBHttpWithTicketHashService):
    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        if args.get("user_name") is None:
            user_name = gcfg.user_name
        else:
            user_name = args.get("user_name")

        if args.get("project_name") is None:
            raise M4DBModelParamException("Variable 'project_name' is required")

        endpoint = "https://{url:}:{port:}/models/summary/{user_name:}/{project_name:}".format(
            url=gcfg.url,
            port=gcfg.port,
            user_name=user_name,
            project_name=args.get("project_name")
        )
        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        return json.loads(http_response.text)


########################################################################################################################
# Update the running status
########################################################################################################################

class SetModelRunningStatus(M4DBHttpWithTicketHashService):

    def __init__(self):
        super().__init__(M4DBHttpServiceType.POST)

    def post(self, ticket_hash, **args):
        gcfg = get_global_variables()

        # Check the inputs
        if args.get("unique_id") is None:
            raise M4DBModelParamException("Variable 'unique_id' is required")
        if args.get("new_running_status") is None:
            raise M4DBModelParamException("Variable 'new_running_status' is required")
        else:
            if args.get("new_running_status") not in gcfg.running_status_names:
                raise M4DBModelParamException(
                    "Variable 'unique_id' has unknown value: '{}'".format(
                        args.get("new_running_status")
                    )
                )

        data = {
            "unique_id": args.get("unique_id"),
            "new_running_status": args.get("new_running_status")
        }

        endpoint = "https://{url:}:{port:}/model/set-running-status".format(
            url=gcfg.url,
            port=gcfg.port,
            data=json.dumps(data)
        )
        http_response = self.http_session.post(
            endpoint,
            verify=self.verify_services,
            headers={
                "Content-Type": "application/json",
                "user_name": gcfg.user_name,
                "ticket_hash": ticket_hash
            },
            data=json.dumps(data)
        )
        http_response.raise_for_status()

########################################################################################################################
# Update the quants
########################################################################################################################


class SetModelQuants(M4DBHttpWithTicketHashService):

    def __init__(self):
        super().__init__(M4DBHttpServiceType.POST)

    def post(self, ticket_hash, **args):
        gcfg = get_global_variables()

        # Check the inputs
        if args.get("unique_id") is None:
            raise M4DBModelParamException("Variable 'unique_id' is required")

        # Build the query object

        quant_fields = [
            "mx_tot",
            "my_tot",
            "mz_tot",
            "vx_tot",
            "vy_tot",
            "vz_tot",
            "h_tot",
            "adm_tot",
            "e_typical",
            "e_anis",
            "e_ext",
            "e_demag",
            "e_exch1",
            "e_exch2",
            "e_exch3",
            "e_exch4",
            "e_tot"
        ]

        data = {}

        for qn in quant_fields:
            if args.get(qn) is not None:
                data[qn] = args.get(qn)

        if len(data) == 0:
            raise M4DBModelParamException("There are no quants to update!")

        # Add the unique id as a final value
        data["unique_id"] = args.get("unique_id")

        # Communicate with the service
        endpoint = "https://{url:}:{port:}/model/set-quants".format(
            url=gcfg.url,
            port=gcfg.port,
            data=json.dumps(data)
        )
        http_response = self.http_session.post(
            endpoint,
            verify=self.verify_services,
            headers={
                "Content-Type": "application/json",
                "user_name": gcfg.user_name,
                "ticket_hash": ticket_hash
            },
            data=json.dumps(data)
        )
        http_response.raise_for_status()

########################################################################################################################
# Service to add new unrun models.
########################################################################################################################

class AddNotRunModel(M4DBHttpWithTicketHashService):

    def __init__(self):
        super().__init__(M4DBHttpServiceType.POST)

    def post(self, ticket_hash, **args):
        gcfg = get_global_variables()

        # Check the parameters