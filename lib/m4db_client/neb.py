r"""
A library to perform M4DB web service requests involving NEB paths.
"""
import re
import json

import numpy as np

from m4db_client.global_config import get_global_variables

from m4db_client.http_session import M4DBHttpServiceType
from m4db_client.http_session import M4DBHttpWithTicketHashService

class M4DBNEBException(Exception):
    r"""
    Base exception for all model exceptions.
    """
    pass


class M4DBNEBParamException(Exception):
    def __init__(self, message):
        self.message = message


class GetNEBPathStructureEnergiesByUniqueId(M4DBHttpWithTicketHashService):
    r"""
    Objects of this class will retrieve a model with a
    specific unique identifier.
    """

    def __init__(self):
        super().__init__(M4DBHttpServiceType.GET)

    def get(self, ticket_hash, **args):
        gcfg = get_global_variables()

        if args.get("unique_id") is None:
            raise M4DBNEBParamException("Variable 'unique_id' is required")

        endpoint = "https://{url:}:{port:}/neb/path-structure-energies/{unique_id:}".format(
            url=gcfg.url,
            port=gcfg.port,
            unique_id=args.get("unique_id")
        )

        http_response = self.http_session.get(
            endpoint,
            verify=self.verify_services,
            headers={"user_name": gcfg.user_name, "ticket_hash": ticket_hash}
        )
        http_response.raise_for_status()

        return np.array(json.loads(http_response.text))
