r"""
A set of useful small utilities.
"""
import os
import datetime
import traceback

DATE_TIME_FORMAT = "%m/%d/%Y, %H:%M:%S"


def file_creation_time(file_name):
    r"""
    Retrieve the creation time of the file as a datetime object.
    :param file_name: the file name for which the creation time is needed.
    :return: the creation time of `file_name`.
    """
    return datetime.datetime.fromtimestamp(os.path.getmtime(file_name))


def str_to_datetime(str_datetime):
    return datetime.datetime.strptime(str_datetime, DATE_TIME_FORMAT)


def datetime_to_str(datetime_obj):
    return datetime_obj.strftime(DATE_TIME_FORMAT)


def print_stack_trace():
    print("----------- STACK TRACE -----------")
    traceback.print_exc()
    print("------- STACK TRACE COMPLETE ------")


