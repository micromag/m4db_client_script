r"""
Basic utilities for templating.
"""
from jinja2 import Environment, PackageLoader, select_autoescape

from m4db_client.decorators import static


@static(loader=None, env=None)
def get_template(name):
    r"""
    Retrieve a JINJA2 template for the given name.
    :param name: the template to retrieve.
    :return: a Jinja2 template
    """
    if get_template.loader is None and get_template.env is None:
        # Create a new loader/environment
        get_template.loader = PackageLoader("m4db_client", "resources/templates")
        get_template.env = Environment(
            loader=get_template.loader,
            autoescape=select_autoescape(['jinja2'])
        )

    return get_template.env.get_template(name)
