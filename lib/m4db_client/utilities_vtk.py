import vtk


def formatFloat(flt):
    strFlt = "{:20.15E}".format(flt)
    if strFlt[0] != '-':
        return ' ' + strFlt
    return strFlt


def pathIdNameM(pathId):
    return 'm_{}'.format(pathId)


def pathIdNameH(pathId):
    return 'h_{}'.format(pathId)


def pathIdNameV(pathId):
    return 'v_{}'.format(pathId)


def pathIdNameADM(pathId):
    return 'adm_{}'.format(pathId)


def tec_to_unstructured_grid(magnetization, adm_fun='0.5*(1-(({M}.iHat)^4 + ({M}.jHat)^4 + ({M}.kHat)^4))'):
    mag = magnetization['fields']
    vert = magnetization['vertices']
    conn = magnetization['elements']

    nmag = magnetization['nfields']
    nvert = magnetization['nvert']
    nconn = magnetization['nelem']

    # print("len(mag): {}".format(len(mag)))

    # The unstructured grid
    ug = vtk.vtkUnstructuredGrid()

    # Vertices.
    # print("Adding vertices")
    points = vtk.vtkPoints()
    points.SetNumberOfPoints(nvert)
    for i in range(nvert):
        points.SetPoint(i, vert[i][0], vert[i][1], vert[i][2])
    ug.SetPoints(points)
    # print("...done!")

    # Connectivity
    # print("Adding elements")
    for i in range(nconn):
        element = vtk.vtkIdList()

        element.InsertNextId(conn[i][0])
        element.InsertNextId(conn[i][1])
        element.InsertNextId(conn[i][2])
        element.InsertNextId(conn[i][3])

        ug.InsertNextCell(vtk.VTK_TETRA, element)
    # print("...done!")

    # Magnetizations in all the zone
    for zone in range(nmag):

        # print("Adding magnetizations (name {})".format(pathIdNameM(zone)))
        field = vtk.vtkDoubleArray()
        field.SetName(pathIdNameM(zone))
        field.SetNumberOfComponents(3)
        field.SetNumberOfTuples(nvert)

        for i in range(nvert):
            field.SetTuple3(i, mag[zone][i][0], mag[zone][i][1], mag[zone][i][2])

        ug.GetPointData().AddArray(field)
        # print("...done!")

        # print("Computing vorticity field")
        vorticity = vtk.vtkGradientFilter()
        vorticity.ComputeVorticityOn()
        vorticity.SetInputArrayToProcess(0, 0, 0, 0, pathIdNameM(zone))
        # vorticity.SetResultArrayName('V')
        vorticity.SetVorticityArrayName('V')
        vorticity.SetInputData(ug)
        vorticity.Update()
        # print("...done!")

        # print("Adding vorticities (name {})".format(pathIdNameV(zone)))
        vfield = vtk.vtkDoubleArray()
        vfield.SetName(pathIdNameV(zone))
        vfield.SetNumberOfComponents(3)
        vfield.SetNumberOfTuples(nvert)

        vdata = vorticity.GetOutput().GetPointData().GetArray('V')
        for i in range(nvert):
            vfield.SetTuple3(i, vdata.GetTuple(i)[0],
                             vdata.GetTuple(i)[1],
                             vdata.GetTuple(i)[2])
        ug.GetPointData().AddArray(vfield)
        # print("..done!")

        # print("Computing helicity field")
        helicity = vtk.vtkArrayCalculator()
        # helicity.SetAttributeModeToUsePointData()
        helicity.AddVectorArrayName(pathIdNameM(zone))
        helicity.AddVectorArrayName('V')
        helicity.SetResultArrayName('H')
        helicity.SetFunction('{}.V'.format(pathIdNameM(zone)))
        helicity.SetInputData(vorticity.GetOutput())
        helicity.Update()
        # print("...done!")

        # print("Adding helicities (name {})".format(pathIdNameH(zone)))
        # helicities = []
        hfield = vtk.vtkDoubleArray()
        hfield.SetName(pathIdNameH(zone))
        hfield.SetNumberOfComponents(1)
        hfield.SetNumberOfTuples(nvert)

        hdata = helicity.GetOutput().GetPointData().GetArray('H')
        for i in range(nvert):
            hfield.SetValue(i, hdata.GetValue(i))
        ug.GetPointData().AddArray(hfield)
        # print("...done!")

        # print("Computing ADM field")
        adm = vtk.vtkArrayCalculator()
        adm.AddVectorArrayName(pathIdNameM(zone))
        adm.SetResultArrayName('ADM')
        adm.SetFunction(adm_fun.format(M=pathIdNameM(zone)))
        adm.SetInputData(vorticity.GetOutput())
        adm.Update()
        # print("...done!")

        # print("Adding ADM (name {})".format(pathIdNameADM(zone)))
        # adms = []
        adm_field = vtk.vtkDoubleArray()
        adm_field.SetName(pathIdNameADM(zone))
        adm_field.SetNumberOfComponents(1)
        adm_field.SetNumberOfTuples(nvert)

        adm_data = adm.GetOutput().GetPointData().GetArray('ADM')
        for i in range(nvert):
            adm_field.SetValue(i, adm_data.GetValue(i))
        ug.GetPointData().AddArray(adm_field)
        # print("...done!")

    return ug
