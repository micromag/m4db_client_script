import os

import yaml

from m4db_client.decorators import static

M4DB_ENV_VAR_NAME = "M4DB_CLIENT_CONFIG"

M4DB_CONFIG_ENTRIES = [
    "backoff", "forcelist", "port", "retries", "url", "work_dir", "user_name"
]


class M4DBConfigurationNoEnvironmentVariableSetException(Exception):
    def __init__(self, message):
        self.message = message


class M4DBConfigurationMissingParameterException(Exception):
    def __init__(self, message):
        self.message = message


@static(config=None)
def read_config_from_file(file_name):
    r"""
    Reads an M4DB database configuration from a file. An example configuration file looks like
        backoff: 0.1
        forcelist: 500,502,504
        port: 8000
        retries: 10
        url: localhost
        work_dir: /home/lnagy2/MMDatabase
        user_name: lnagy2

    Args:
        file_name: the M4DB configuration file.

    Returns:
        A python dictionary representation of M4DB database related configuration information.

    """
    if read_config_from_file.config is None:
        with open(file_name, "r") as fin:
            read_config_from_file.config = yaml.load(fin, Loader=yaml.FullLoader)
            for entry in M4DB_CONFIG_ENTRIES:
                if entry not in read_config_from_file.config.keys():
                    raise M4DBConfigurationMissingParameterException(
                        "Configuration is missing required parameter '{}'.".format(entry)
                    )
    return read_config_from_file.config


def read_config_from_environ():
    r"""
    Reads an M4DB database configuration by checking for an environment variable called 'M4DB_CONF_DATABASE".

    Returns:
        A python dictionary representation of M4DB database related configuration information.

    """
    file_name = os.environ.get(M4DB_ENV_VAR_NAME)

    if file_name is None:
        raise M4DBConfigurationNoEnvironmentVariableSetException(
            "{} environment variable doesn't exist.".format(M4DB_ENV_VAR_NAME)
        )
    else:
        return read_config_from_file(file_name)


def write_config_to_file(file_name, config):
    r"""
    Writes M4DB database configuration.

    file_name: the file name to write configuration data to.
    config: A python dictionary representation of M4DB database related configuration information.

    Returns:
        None.

    """
    with open(file_name, "w") as fout:
        yaml.dump(config, fout, default_flow_style=False)


def write_config_to_environ(config):
    r"""
    Writes an m4db client configuration by checking fo an environment variable called M4DB_CLIENT_CONFIG.

    config: a python directory representation of the configuration.

    Returns: None
    """
    file_name = os.environ.get(M4DB_ENV_VAR_NAME)

    if file_name is None:
        raise M4DBConfigurationNoEnvironmentVariableSetException(
            "{} environment variable doesn't exist.".format(M4DB_ENV_VAR_NAME)
        )
    else:
        write_config_to_file(file_name, config)

