#!/usr/scripts/env python

from distutils.core import setup

NAME = "m4db_client_script"
VERSION = "1.0.0"

setup(name=NAME,
      version=VERSION,
      license="MIT",
      description="M4DB client utilities",
      url="https://bitbucket.org/micromag/{name:}/src/master/".format(name=NAME),
      download_url="https://bitbucket.org/micromag/{name:}/get/{name:}-{version:}.zip".format(name=NAME, version=VERSION),
      keywords=["micromagnetics", "database"],
      install_requires=["PyYAML",],
      author="L. Nagy, W. Williams",
      author_email="lnagy2@ed.ac.uk",
      packages=["m4db_client", "m4db_client.resources"],
      package_dir={"m4db_client": "lib/m4db_client", "m4db_client.resources": "lib/m4db_client/resources"},
      scripts=[
            "scripts/m4db_client_config",
            "scripts/m4db_geometry",
            "scripts/m4db_model",
            "scripts/m4db_user",
            "scripts/m4db_software"
      ],
      package_data = {
            "m4db_client": ["resources/config.yaml", "resources/server.cert", "resources/templates/*.jinja2"]
      },
      classifiers=[
            "Development Status :: 3 - Alpha",
            "Intended Audience :: Developers",
            "Topic :: Database",
            "License :: OSI Approved :: MIT License",
            "Programming Language :: Python :: 3",
            "Programming Language :: Python :: 3.6",
            "Programming Language :: Python :: 3.7",
            "Programming Language :: Python :: 3.8",
      ]),
